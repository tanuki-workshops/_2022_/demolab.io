const fs = require("fs")

let severityValues = ["info", "minor", "major", "critical", "blocker"]

let hotSpots = [
    {
        description: "Too long string",
        fingerprint: "001",
        location: {
            path: "index/main.go",
            lines: {
                begin: 40
            }
        },
        severity: severityValues[1]
    },
    {
        description: "Usage of interface{} is not recommended",
        fingerprint: "002",
        location: {
            path: "hello/main.go",
            lines: {
                begin: 11
            }
        },
        severity:severityValues[0]
    }
    ,
    {
        description: "Usage of interface{} is not recommended",
        fingerprint: "003",
        location: {
            path: "morgen/main.go",
            lines: {
                begin: 11
            }
        },
        severity:severityValues[0]
    }
]

fs.writeFileSync("./gl-code-quality-report.json", JSON.stringify(hotSpots, null, 2))
